# Author: Carles Mateo

from carleslibs.subprocessutils import SubProcessUtils
from carleslibs.datetimeutils import DateTimeUtils
from tkinter import *
from tkinter import ttk


class ZpoolWatch:

    s_version = "0.2"

    def __init__(self, o_datetime, o_subprocess, i_sleep_seconds=30):

        self.o_datetime = o_datetime
        self.o_subprocess = o_subprocess
        self.i_sleep_seconds = i_sleep_seconds

    def prepare_p(self, s_text):
        s_datetime = self.o_datetime.get_datetime(b_milliseconds=False, b_remove_spaces_and_colons=False, b_remove_dashes=False)
        s_epoch = self.o_datetime.get_unix_epoch()

        s_line = s_datetime + " " + s_epoch + " " + s_text

        return s_line

    def p(self, s_text):
        """
        Prints the s_text with the Datetime and Unix Epoch Time preceding
        :param s_text:
        :return:
        """
        s_line = self.prepare_p(s_text)
        print(s_line)

    def is_zpool_healthy(self, s_zpool_status_output):
        """"
        Returns True if the zpool is healthy, False otherwise
        """
        if "DEGRADED" in s_zpool_status_output or "OFFLINE" in s_zpool_status_output:
            # The zpool has some kind of problem
            return False

        return True

    def show_window(self, s_message):
        o_root = Tk()
        frm = ttk.Frame(o_root, padding=10)
        frm.grid()
        s_window_title = "zpool watch" + " v." + self.s_version
        o_root.title(string=s_window_title)
        ttk.Label(frm, text=s_message).grid(column=0, row=0)
        ttk.Button(frm, text="Close", command=o_root.destroy).grid(column=1, row=0)
        o_root.mainloop()

    def run(self):
        s_command = "zpool status"
        self.p("Watching zpool with: " + s_command + " every " + str(self.i_sleep_seconds) + " seconds")
        try:
            while True:
                i_error_code, s_output, s_error = self.o_subprocess.execute_command_for_output(s_command, b_shell=True, b_convert_to_ascii=True, b_convert_to_utf8=False)
                s_message = "Exit Code: " + str(i_error_code)
                if self.is_zpool_healthy(s_output) is False:
                    s_message = s_message + " Problem found"
                    s_message = s_message + "\n" + s_output

                    s_window_message = self.prepare_p(s_message)
                    self.p(s_message)
                    self.show_window(s_window_message)
                else:
                    s_message = s_message + " No problems detected"
                    self.p(s_message)

                self.o_datetime.sleep(self.i_sleep_seconds)

        except Exception as e:
            print(e)
        except KeyboardInterrupt:
            print("CTRL + C pressed, exiting")


if __name__ == "__main__":

    o_datetime = DateTimeUtils()
    o_subprocess = SubProcessUtils()

    o_zwatch = ZpoolWatch(o_datetime=o_datetime, o_subprocess=o_subprocess, i_sleep_seconds=30)
    o_zwatch.run()
