# zpool_watcher

By Carles Mateo

v.0.2

https://blog.carlesmateo.com/zpool-watch/

## Getting started

zpool_watcher is a little Open Source tool for Linux Desktop that monitors an OpenZFS zpool and displays a message when an error is detected or the Pool is DEGRADED.

It uses tkinter for displaying the window.
https://docs.python.org/3/library/tkinter.html

It also uses my Open Source package carleslibs. You can install them in Ubuntu with:

```
pip3 install -r requirements.txt
```

or simply:

```
pip3 install carleslibs
```
